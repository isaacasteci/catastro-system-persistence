package mx.com.dos.catastrosystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatastroSystemPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatastroSystemPersistenceApplication.class, args);
	}

}
